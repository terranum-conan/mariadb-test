from conans import ConanFile, CMake
import os


class ToolbasView(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = [
        "mariadb/10.6.10@terranum-conan+mariadb/stable",
        "wxwidgets/3.2.4@terranum-conan+wxwidgets/stable",
        "gtest/1.14.0",
    ]

    options = {"unit_test": [True, False]}
    default_options = {"unit_test": False}

    generators = "cmake", "gcc", "txt"

    def configure(self):
        if self.settings.os == "Linux":
            self.options["wxwidgets"].png = "system"

    def imports(self):
        # copy libraries
        self.copy("*.dll", dst="bin", src="bin")  # From bin to bin
        self.copy("*.dylib", dst="bin", src="lib")
        if self.settings.os == "Linux":
            self.copy("*.so*", dst="bin", src="lib")

        # copy errmsg.sys on different places
        if self.settings.os == "Windows" or self.settings.os == "Linux":
            self.copy("errmsg.sys", dst="bin/mysql", src="share/english")
        if self.settings.os == "Macos":
            # self.copy("errmsg.sys", dst="bin/ToolBasView.app/Contents/mysql", src="share/english")
            self.copy("errmsg.sys", dst="bin/mysql", src="share/english")

        if self.options.unit_test:
                self.copy("errmsg.sys", dst="mysql", src="share/english")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        if self.settings.os == "Macos":
            cmake.install()
        if self.settings.os == "Linux":
            self.cpp_info.libs.append("pcre")
            self.output.info("Adding libraries......")
