#include "wx/wxprec.h"

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <wx/app.h>
#include <wx/cmdline.h>
#include <wx/filename.h>  // to create the database path and name.
#include <wx/stdpaths.h>  // std path for logging too.

#include "database.h"
#include "wx/init.h"

int main(int argc, char** argv) {
    wxInitializer ini;
    wxApp::CheckBuildOptions(WX_BUILD_OPTIONS_SIGNATURE, "program");
    wxPrintf("mariadb version: %d.%d.%d\n", MYSQL_VERSION_MAJOR, MYSQL_VERSION_MINOR, MYSQL_VERSION_PATCH);

    // data path
    wxFileName f(wxStandardPaths::Get().GetExecutablePath());
    wxFileName my_data_path(f.GetPath(), "");
    my_data_path.AppendDir("data");
    if (!my_data_path.IsDirReadable()) {
        wxLogError(_("Directory : %s doesn't exists or isn't readable"), my_data_path.GetFullPath());
        return (1);
    }
    wxPrintf("Path is: %s", my_data_path.GetFullPath() + "\n");

    Database db(my_data_path.GetFullPath());
    if (!db.CreateNewDatabase("testnewDB")) {
        wxPrintf("Error creating DB!");
        return 1;
    }

    if (!db.OpenDatabase("testnewDB")) {
        wxPrintf("Error opening DB");
        return 1;
    }

    return 0;
}
