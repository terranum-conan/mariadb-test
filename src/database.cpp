
#include "database.h"
Database::Database(const wxString &database_dir) {
  m_db = nullptr;
  m_results = nullptr;
  m_database_directory = database_dir;
  m_is_database_open = false;

  m_lib_inited = _LibraryInit();
}

Database::~Database() {
  _ConnectionClose();
  if (m_lib_inited) {
    mysql_library_end();
  }
}

bool Database::_LibraryInit() {
  if (!wxDirExists(m_database_directory)) {
    wxLogError("data directory '%s' didn't exist!", m_database_directory);
    return false;
  }

  // init library parameters
  wxFileName f(wxStandardPaths::Get().GetExecutablePath());
  wxFileName windows_msg_path(f.GetPath(), "mysql", "");
  wxString mylanguagedir = _T("--lc-messages-dir=") + windows_msg_path.GetFullPath();
  wxLogDebug("language dir: %s\n", mylanguagedir);

  // server settings
  wxArrayString server_args_array;
  server_args_array.Add("this_program");  // this string is not used but mandatory
  server_args_array.Add("--datadir=" + m_database_directory);
  server_args_array.Add(mylanguagedir);
  server_args_array.Add("--character-set-server=utf8");
  server_args_array.Add("--default-storage-engine=MyISAM");
  server_args_array.Add("--default-tmp-storage-engine=MyISAM");
  // server_args_array.Add("--skip-innodb");

  char *my_args[6];
  for (int i = 0; i < server_args_array.GetCount(); i++) {
    my_args[i] = const_cast<char *>((const char *)server_args_array[i].mb_str());
  }

  static const char *server_groups[] = {"embedded", "server", "this_program_SERVER", (char *)nullptr};
  int myReturn = mysql_library_init(server_args_array.GetCount(), &my_args[0], const_cast<char **>(server_groups));
  if (myReturn != 0) {
    wxLogError("%d error starting library...", myReturn);
    return false;
  }
  return true;
}

void Database::_ConnectionInit() {
  if (!m_lib_inited) {
    return;
  }

  m_db = mysql_init(nullptr);
  mysql_options(m_db, MYSQL_OPT_USE_EMBEDDED_CONNECTION, nullptr);
  mysql_options(m_db, MYSQL_SET_CHARSET_NAME, "utf8");
}

void Database::_ConnectionClose() {
  if (!m_lib_inited) {
    return;
  }

  if (m_db) {
    mysql_close(m_db);
    m_db = nullptr;
  }
}

bool Database::CreateNewDatabase(const wxString &database_name) {
  m_database_name = database_name;
  m_is_database_open = false;
  if (!m_lib_inited) {
    return false;
  }

  _ConnectionInit();
  if (mysql_real_connect(m_db, NULL, NULL, NULL, NULL, 0, NULL, 0) == NULL) {
    wxLogError("%s\n", mysql_error(m_db));
    _ConnectionClose();
    return false;
  }

  // Create a sample empty DB, named "aNewDatabase"
  wxString my_query = wxString::Format("CREATE DATABASE %s", m_database_name);
  if (mysql_query(m_db, my_query.mb_str(wxConvUTF8))) {
    wxPrintf("%s\n", mysql_error(m_db));
    _ConnectionClose();
    return false;
  }
  _ConnectionClose();
  return true;
}

bool Database::OpenDatabase(const wxString &database_name) {
  m_database_name = database_name;
  m_is_database_open = false;
  if (!m_lib_inited) {
    return false;
  }

  wxFileName my_full_path(m_database_directory, m_database_name);
  if (!wxDirExists(my_full_path.GetFullPath())) {
    wxLogError("Database: '%s' didn't exists!", my_full_path.GetFullPath());
    return false;
  }

  _ConnectionInit();
  if (mysql_real_connect(m_db, NULL, NULL, NULL, (const char *)database_name.mb_str(wxConvUTF8), 0, NULL,
                         CLIENT_MULTI_STATEMENTS) == NULL) {
    wxPrintf("%s\n", mysql_error(m_db));
    _ConnectionClose();
    return false;
  }
  m_is_database_open = true;
  return true;
}

bool Database::Query(const wxString &query, bool keep_result) {
  if (!m_is_database_open) {
    wxLogError("Database isn't open!");
    return false;
  }

  wxASSERT(m_db);
  if (mysql_query(m_db, query.mb_str(wxConvUTF8)) != 0) {
    wxLogError("%s\n", mysql_error(m_db));
    return false;
  }

  m_results = mysql_store_result(m_db);
  if (!keep_result) {
    mysql_free_result(m_results);
    m_results = nullptr;
  }
  return true;
}
