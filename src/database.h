#ifndef MARIADB_TEST_DATABASE_H
#define MARIADB_TEST_DATABASE_H

#include "wx/wxprec.h"
#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif
#include <wx/filename.h>  // to create the database path and name.

#include <my_global.h>
#include <mysql.h>
#include <wx/stdpaths.h>  // std path for logging too.


class Database {
 public:
  Database(const wxString & database_dir);
  ~Database();
  bool CreateNewDatabase(const wxString & database_name);
  bool OpenDatabase(const wxString & database_name);
  bool Query(const wxString & query, bool keep_result=false);

 private:

  bool _LibraryInit();
  void _ConnectionInit();
  void _ConnectionClose();

  wxString m_database_directory;
  wxString m_database_name;
  MYSQL * m_db;
  MYSQL_RES * m_results;
  bool m_lib_inited;
  bool m_is_database_open;
};

#endif
