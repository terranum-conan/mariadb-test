#include "database.h"
#include "gtest/gtest.h"
#include "test_param.h"


class TestDatabase : public ::testing::Test {
 protected:
  void ClearDirectoryIfNeeded(const wxString & path, const wxString & name){
    wxFileName my_file_to_clean (path, "");
    my_file_to_clean.AppendDir(name);
    wxLogWarning(my_file_to_clean.GetFullPath());
    if (my_file_to_clean.Exists()){
      my_file_to_clean.Rmdir(wxPATH_RMDIR_RECURSIVE);
    }
    ASSERT_FALSE(my_file_to_clean.Exists());
  }

  static void SetUpTestSuite() {
    // create the output directory
    wxFileName output_path(UNIT_TESTING_DATA_OUTPUT_PATH, wxEmptyString);
    if (!output_path.Exists()) {
      ASSERT_TRUE(output_path.Mkdir());
    }
    m_db = new Database(UNIT_TESTING_DATA_OUTPUT_PATH);
  }
  static void TearDownTestSuite() {
    wxDELETE(m_db);
  }
  static Database* m_db;
};

Database * TestDatabase::m_db = nullptr;

// Disabled because of a bug in MariaDB
TEST_F(TestDatabase, DISABLED_CreateDatabaseMulti) {
  ClearDirectoryIfNeeded(UNIT_TESTING_DATA_OUTPUT_PATH, "testdatabase");
  Database m_db1(UNIT_TESTING_DATA_OUTPUT_PATH);
  ASSERT_TRUE(m_db1.CreateNewDatabase("testdatabase"));
}

// Disabled because of a bug in MariaDB
TEST_F(TestDatabase, DISABLED_OpenDatabaseMulti) {
  Database m_db2(UNIT_TESTING_DATA_OUTPUT_PATH);
  ASSERT_TRUE(m_db2.OpenDatabase("testdatabase"));
}

TEST_F(TestDatabase, CreateDatabase) {
  ClearDirectoryIfNeeded(UNIT_TESTING_DATA_OUTPUT_PATH, "testdb2");
  ClearDirectoryIfNeeded(UNIT_TESTING_DATA_OUTPUT_PATH, "testdb1");
  ASSERT_TRUE(m_db->CreateNewDatabase("testdb2"));
  ASSERT_TRUE(m_db->CreateNewDatabase("testdb1"));

}

TEST_F(TestDatabase, OpenDatabase) {
  ASSERT_TRUE(m_db->OpenDatabase("testdb2"));
}

TEST_F(TestDatabase, OpenAnotherDatabase) {
  ASSERT_TRUE(m_db->OpenDatabase("testdb1"));
}

TEST_F(TestDatabase, OpenDatabaseNotExisting) {
  ASSERT_FALSE(m_db->OpenDatabase("testnotexist"));
}

TEST_F(TestDatabase, QueryFailed) {
  ASSERT_FALSE(m_db->Query(
      "CREATE TABLE IF NOT EXISTS tasks (\n"
      "    task_id INT AUTO_INCREMENT PRIMARY KEY,\n"
      "    title VARCHAR(255) NOT NULL,\n"
      "    start_date DATE,\n"
      "    due_date DATE,\n"
      "    status TINYINT NOT NULL,\n"
      "    priority TINYINT NOT NULL,\n"
      "    description TEXT,\n"
      "    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP\n"
      ")  ENGINE=MyISAM;",
      false));
}

TEST_F(TestDatabase, QueryCreateTable) {
  ASSERT_TRUE(m_db->OpenDatabase("testdb1"));
  ASSERT_TRUE(m_db->Query(
      "CREATE TABLE IF NOT EXISTS tasks (\n"
      "    task_id INT AUTO_INCREMENT PRIMARY KEY,\n"
      "    title VARCHAR(255) NOT NULL,\n"
      "    start_date DATE,\n"
      "    due_date DATE,\n"
      "    status TINYINT NOT NULL,\n"
      "    priority TINYINT NOT NULL,\n"
      "    description TEXT,\n"
      "    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP\n"
      ")  ENGINE=MyISAM;",
      false));
}